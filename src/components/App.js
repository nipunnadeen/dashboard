import React, { Component } from 'react';
import Dashboard from './dashboard/Dashboard';

class App extends Component {

  render() {
    return (
      <Dashboard />
    );
  }
}

export default App;

