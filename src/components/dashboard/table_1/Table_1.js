import React, { Component } from 'react';
import "./Table_1.css";
import config from "../../../resources/config.json";
import Dashboard from "../Dashboard";

/**
 * @desc this component get the issues details from the parent component using props values and do the calculation and then
 fill the table with them.
 */
class Table_1 extends Component {
    updated_at;
    web_url;
    table1_issues_details;

    getTable1Data(){

                const {table1 } = config;
                const list = [];
                const details = this.props.table1_issues_details;

                details.forEach((details) => {
                    const detailsLabels = details.labels;
                    const format_date_time = details.updated_at;
                    const url = details.web_url.split( '/' );
                    const duration = Dashboard.getDuration(format_date_time);
                    const filtered_data = {};

                    function get_common_data(){
                        filtered_data['id']=details.id;
                        filtered_data['iid']=details.iid;
                        filtered_data['customer_acc']=url[4];
                        filtered_data['title']=details.title;
                        filtered_data['time']=Dashboard.formatDateTime(format_date_time);
                    }

                    //this condition check the issues which has no assignee and the labels
                    //this conditions for get the open issues
                    if(details.labels.length===0 && details.assignee ===null){
                        if(duration>=table1.open_time_duration){
                            get_common_data();
                            filtered_data['label']="Open";
                            filtered_data['assignee']="No Assignee";

                            list.push(filtered_data);
                        }
                    }
                    detailsLabels.forEach((detailsLabels) => {

                        table1.search_labels.forEach((table1_label_data) => {
                            //this conditions for get the W.o.E issues
                            if (detailsLabels === table1_label_data) {
                                //check the time duration of the issue
                                if (duration >= table1.woe_time_duration) {

                                    if (details.assignee === null) {
                                        get_common_data();
                                        filtered_data['label'] = detailsLabels;
                                        filtered_data['assignee'] = "No Assignee";

                                        list.push(filtered_data);

                                    } else if (details.assignee !== null) {
                                        get_common_data();
                                        filtered_data['label'] = detailsLabels;
                                        filtered_data['assignee'] = details.assignee.name;

                                        list.push(filtered_data);
                                    }
                                }
                            }
                        });
                    });
                });
                const rows = list.map((list) => (

                    <tr key={list.id}>
                        <td>{list.customer_acc} #{list.iid}</td>
                        <td>{list.title}</td>
                        <td>{list.label}</td>
                        <td>{list.assignee}</td>
                        <td>{list.time}</td>
                    </tr>
                ));

        const table1_structure =(
            <thead>
            <tr>
                <th>Issue</th>
                <th>Title</th>
                <th>State</th>
                <th>Assignee</th>
                <th>Update Time</th>
            </tr>
            </thead>);
                //check the list is empty or not, if its not the value will return to the table
                if(list.length !==0){
                    return (
                        <table>
                            {table1_structure}
                            <tbody>{rows}</tbody>
                        </table>
                    )
                }else{
                    return (
                        <div>
                            <table>
                                {table1_structure}
                            </table>
                            <div className="table1_empty">
                                <h5>No Data</h5>
                            </div>
                        </div>
                    )
                }
    };
    render() {
        return (
            <div>
                <div className="title1">
                    <h4>Open & W.O.E Issues</h4>
                </div>
                <div className="table1">{this.getTable1Data()}</div>
            </div>
        );
    }
}

export default Table_1;

