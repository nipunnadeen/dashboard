import React, { Component } from 'react';
import "./Table_2.css";
import config from "../../../resources/config.json";
import Dashboard from "../Dashboard";

/**
 *  @desc this component get the issues details from the parent component using props values and do the calculation and then
 fill the table with them.
 */
class Table_2 extends Component {
    table2_issues_details;

    getTable2Data(){

                const {table2} = config;
                const table2_list = [];
                //it gets the props values from the dashboard component
                const details = this.props.table2_issues_details;

            details.forEach((details) => {

                    const format_time = details.updated_at;
                    const url = details.web_url.split( '/' );
                    const filtered_data = {};

                    function get_common_data(){
                        filtered_data['id']=details.id;
                        filtered_data['iid']=details.iid;
                        filtered_data['customer_acc']=url[4];
                        filtered_data['title']=details.title;
                        filtered_data['time']=Dashboard.formatDateTime(format_time);
                    }

                    details.labels.forEach((detailsLabels) => {

                        //get the data of the json lists
                        table2.search_labels.forEach((table2_label_data) => {
                                if (detailsLabels === table2_label_data) {
                                    // check, the each issue has a assignee or not
                                    if (details.assignee === null) {
                                        get_common_data();
                                        filtered_data['label'] = detailsLabels;
                                        filtered_data['assignee'] = "No Assignee";

                                        table2_list.push(filtered_data);

                                    } else if (details.assignee !== null) {
                                        get_common_data();
                                        filtered_data['label'] = detailsLabels;
                                        filtered_data['assignee'] = details.assignee.name;

                                        table2_list.push(filtered_data);
                                    }
                                }
                            });
                    });
                });
                const rows = table2_list.map((list) => (

                    <tr key={list.id}>
                        <td>{list.customer_acc} #{list.iid}</td>
                        <td>{list.title}</td>
                        <td>{list.label}</td>
                        <td>{list.assignee}</td>
                        <td>{list.time}</td>
                    </tr>
                ));

                const table2_structure =(
                <thead>
                <tr>
                    <th>Issue</th>
                    <th>Title</th>
                    <th>State</th>
                    <th>Assignee</th>
                    <th>Update Time</th>
                </tr>
                </thead>);
                 //check the list is empty or not, if its not the value will return to the table
                if(table2_list.length !==0){
                    return (
                        <table>
                             {table2_structure}
                             <tbody>{rows}</tbody>
                        </table>
                    )
                }else{
                    return (
                        <div>
                            <table>
                                {table2_structure}
                            </table>
                            <div className="table2_empty">
                                <h5>No Data</h5>
                            </div>
                        </div>
                    )
                }
    };
    render() {
        return (
            <div className="table2_body">
                <div className="title2">
                    <h4>In Progress & W.O.C Issues</h4>
                </div>
            <div className="table2">{this.getTable2Data()}</div>
            </div>
        );
    }
}
export default Table_2;

