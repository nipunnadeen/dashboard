import React, { Component } from 'react';
import Chart from "./chart/Chart.js";
import "./Dashboard.css";
import Table1 from "./table_1/Table_1.js";
import Table2 from "./table_2/Table_2.js";
import Table3 from "./table_3/Table_3.js";
import config from "../../resources/config.json";
import axios from "axios";
import moment from 'moment';
import logo from '../../resources/images/logo.png';

/**
 * @desc dashboard component is the parent component.It fetch the issues api and pass the data to child components
   using props
 */
class Dashboard extends Component {

    constructor(props) {
        super(props);
        this.state = {
            issuesDetails:[],
        };
    };
    componentDidMount ()  {
        this.getData(setInterval(this.getData,config.time));
    };
    getData = () => {
        const {token,common_link} = config;
        axios.get(common_link + 'issues?state=opened&per_page=100&private_token=' + token)
            .then(res => {
                const details = res.data;
                this.setState({issuesDetails:details});
            })
            .catch(function (error) {
                console.log(error)
            })
    };

    /**
     * @desc formatDateTime - format the the given date and time
     * @param string string - to pass the argument
     * @return string - return the formated date and time
     */
    static formatDateTime(string){
        const options = { year:'numeric',month:'long',day:'numeric',hour:'numeric',minute:'numeric'};
        return new Date(string).toLocaleDateString([],options);
    };


    /**
     * @desc getDuration - calculate the duration using current time and the given time
     * @param  past_Time - to pass the argument
     * @return return the duration between two times
     */
    static getDuration(past_Time){
        const different = moment(new Date(),"YYYY/MM/DD HH:mm:ss").diff(moment(past_Time,"YYYY/MM/DD HH:mm:ss"));
        return different/(60000*60);
    }

  render(){
      return (
          <div className="dashboard_body">
              <div className="header">
                  <img src={logo} width="148px" height="50px" alt={logo} />
              </div>
              <div className="features">
                  <div className="first_row">
                      <div className="chart">
                          <Chart chart_details={this.state.issuesDetails}/>
                      </div>
                      <div className="table_3">
                          <Table3 table3_issues_details={this.state.issuesDetails}/>
                      </div>
                  </div>
                  <div className="clear-fix" />
                  <div className="second_row">
                      <div className="table_1">
                          <Table1 table1_issues_details={this.state.issuesDetails} />
                      </div>
                      <div className="table_2">
                          <Table2 table2_issues_details={this.state.issuesDetails} />
                      </div>
                  </div>
              </div>
          </div>
      );
  }
}
export default Dashboard;