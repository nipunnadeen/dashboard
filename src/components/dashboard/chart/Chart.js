import React, { Component } from 'react';
import axios from 'axios';
import {Doughnut} from 'react-chartjs-2';
import 'chartjs-plugin-labels';
import "./Chart.css";
import config from "../../../resources/config.json";

/**
 * @desc this component is for show the details on the chart.
 * this component get the issues counts according to labels and the labels details.
 */
class Chart extends Component {
    statistics;
    counts;
    opened;
    open_issues_count;

    constructor(props) {
        super(props);
        this.state = {
            all_opened_issues: 0,
            open_issues: 0,
            chartData: { chartData: null },
        };
    }
    componentDidMount(){
        this.getData(setInterval(this.getData, config.time));
    }

    /**
     * @desc this function fetch the data,do calculation and then pass them to chart to show on the screen
     */
    getData = () => {

        const { token, common_link,open_status,other_status,chart} = config;
        const labelList = [];
        const valueList = [];
        const colorList = [];

        let sum = 0;

        //get the count of opened issues from the issues statistic api
        axios.get(common_link + 'issues_statistics?private_token=' + token)
            .then(res => {
                this.setState({ all_opened_issues: res.data.statistics.counts.opened });
            })
            .catch(function (error) {
                console.log(error);
            });

        //get the count of none label opened issues from the issues statistic api
        axios.get(common_link + 'issues_statistics?labels=None&private_token=' + token)
            .then(res => {
                this.setState({ open_issues: res.data.statistics.counts.opened });
            })
            .catch(function (error) {
                console.log(error);
            });

        //it gives the details of labels from labels api
        axios.get(common_link + 'labels?private_token=' + token)
            .then(res => {
                const issues_data = res.data;
                issues_data.forEach((issues_data) => {

                    //iterate the json label names list and get the values
                    for (let y = 0; y <= chart.show_labels.length; y++) {
                        //check, the labels names are equal
                        if (issues_data.name === chart.labels[y]) {
                            labelList.push(chart.show_labels[y]);
                            valueList.push(issues_data.open_issues_count);
                            colorList.push(issues_data.color);
                        }
                    }

                    //iterate the json main state list and get the values
                    // this value is for get the other issues count
                    for (let x = 0; x <= other_status.labels.length; x++) {
                        if (issues_data.name === other_status.labels[x] ) {
                            sum += issues_data.open_issues_count;
                        }
                    }
                });

                //this condition gets the open issues count and push it to the list
                //check, the json value is true then the values will push to the list
                if (open_status.enable === true) {
                    valueList.push(this.state.open_issues);
                    labelList.push("Open");
                    colorList.push(open_status.color);
                }else{
                    console.log('no open issues');
                }

                 // other issues mean the issues with the label which in the open state mistakenly eg:- Bug,Patch...
                if(other_status.enable === true) {
                    const other_count= this.state.all_opened_issues-this.state.open_issues-sum;
                    valueList.push(other_count);
                    labelList.push("Other");
                    colorList.push(other_status.color);
                }else{
                    console.log('no other issues');
                }

                //set the list data to chart
                this.setState({
                    chartData: {
                        labels: labelList,
                        datasets: [
                            {
                                data: valueList,
                                backgroundColor: colorList,
                            }],
                    }
                })
            })
    };
    render() {
        return (
            <div>
                <div className="total">
                    <div><b>Total Issues: {this.state.all_opened_issues}</b></div>
                </div>
                <div className="chart">
                    <Doughnut
                        data={this.state.chartData}
                        width={500}
                        height={460}
                        options={{
                            responsive: false,
                            maintainAspectoRatio: true,
                            layout: {
                                padding: {
                                    top: 5,
                                    left: 5,
                                    right: 5,
                                    bottom: 5
                                }
                            },
                            animation: {
                                animateScale: true,
                                animateRotate: true,
                                duration: 2000,
                            },
                            plugins: {
                                labels: {
                                    render: 'value',
                                    fontSize: 18,
                                    fontColor: '#000',
                                }
                            },
                            legend: {
                                display: true,
                                position: 'right',
                                labels: {
                                    hidden: 'true',
                                    fontSize: 14,
                                    fontColor: 'black',
                                    usePointStyle: true,
                                    padding: 12,
                                },
                                value: {
                                    display: true,
                                },
                            }
                        }}
                    />
                </div>
            </div>
        );
    }
}

export default Chart;