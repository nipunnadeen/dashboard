import React, { Component } from 'react';
import "./Table_3.css";
import config from "../../../resources/config.json";
import Dashboard from "../Dashboard";

/**
 * @desc this component get the issues details from the parent component using props values and do the calculation and then
   fill the table with them.
 */

class Table_3 extends Component {
    table3_issues_details;

    getTable3Data(){

                const {table3} = config;
                const list = [];
                const details = this.props.table3_issues_details;

                details.forEach((details) => {

                    const format_time = details.updated_at;
                    const url = details.web_url.split( '/' );
                    const duration = Dashboard.getDuration(format_time);
                    const filtered_data = {};

                    //this condition check the issues which has no assignee and the labels
                    if(details.labels.length===0 && details.assignee ===null){
                        if(duration<=table3.open_time_duration){

                            filtered_data['id']=details.id;
                            filtered_data['iid']=details.iid;
                            filtered_data['customer_acc']=url[4];
                            filtered_data['title']=details.title;
                            filtered_data['label']="Open";
                            filtered_data['time']=Dashboard.formatDateTime(format_time);

                            list.push(filtered_data);
                        }
                    }
                });
                const rows = list.map((list) => (
                    <tr key={list.id}>
                        <td>{list.customer_acc} #{list.iid}</td>
                        <td>{list.title}</td>
                        <td>{list.label}</td>
                        <td>{list.time}</td>
                    </tr>
                ));

            const table3_structure =(
            <thead>
            <tr>
                <th>Issue</th>
                <th>Title</th>
                <th>State</th>
                <th>Update Time</th>
            </tr>
            </thead>);
                //check the list is empty or not, if its not the value will return to the table
                if(list.length !==0){
                    return (
                        <table>
                            {table3_structure}
                            <tbody>{rows}</tbody>
                        </table>
                    )
                }else{
                    return (
                        <div>
                        <table>
                            {table3_structure}
                        </table>
                        <div className="table3_empty">
                        <h5>No Data</h5>
                        </div>
                        </div>
                    )
                }
    };

    render() {
        return (
            <div>
            <div className="title3">
            <h4>Open Issues</h4>
            </div>
            <div className="table3">{this.getTable3Data()}</div>
            </div>
        );
    }
}

export default Table_3;

